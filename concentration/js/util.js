//var cards = [
//    {
//      id : 0,
//      src : '/img/HTML5_logo.png'
//    }, {
//      id : 1,
//      src : '/img/CSS_Logo.png'
//    }, {
//      id : 2,
//      src : '/img/JavaScript_logo.png'
//    }, {
//      id : 3,
//      src : '/img/PHP_logo.png'
//    }
//  ];
/*global window */
/*global document */
var cards = [
  {
    id: 0,
    src: '/img/Cards/PNG/10_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk10'
  },
  {
    id: 1,
    src: '/img/Cards/PNG/10_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd10'
  },
  {
    id: 2,
    src: '/img/Cards/PNG/10_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd10'
  },
  {
    id: 3,
    src: '/img/Cards/PNG/10_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk10'
  },
  {
    id: 4,
    src: '/img/Cards/PNG/2_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk2'
  },
  {
    id: 5,
    src: '/img/Cards/PNG/2_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd2'
  },
  {
    id: 6,
    src: '/img/Cards/PNG/2_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd2'
  },
  {
    id: 7,
    src: '/img/Cards/PNG/2_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk2'
  },
  {
    id: 8,
    src: '/img/Cards/PNG/3_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk3'
  },
  {
    id: 9,
    src: '/img/Cards/PNG/3_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd3'
  },
  {
    id: 10,
    src: '/img/Cards/PNG/3_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd3'
  },
  {
    id: 11,
    src: '/img/Cards/PNG/3_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk3'
  },
  {
    id: 12,
    src: '/img/Cards/PNG/4_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk4'
  },
  {
    id: 13,
    src: '/img/Cards/PNG/4_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd4'
  },
  {
    id: 14,
    src: '/img/Cards/PNG/4_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd4'
  },
  {
    id: 15,
    src: '/img/Cards/PNG/4_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk4'
  },
  {
    id: 16,
    src: '/img/Cards/PNG/5_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk5'
  },
  {
    id: 17,
    src: '/img/Cards/PNG/5_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd5'
  },
  {
    id: 18,
    src: '/img/Cards/PNG/5_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd5'
  },
  {
    id: 19,
    src: '/img/Cards/PNG/5_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk5'
  },
  {
    id: 20,
    src: '/img/Cards/PNG/6_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk6'
  },
  {
    id: 21,
    src: '/img/Cards/PNG/6_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd6'
  },
  {
    id: 22,
    src: '/img/Cards/PNG/6_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd6'
  },
  {
    id: 23,
    src: '/img/Cards/PNG/6_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk6'
  },
  {
    id: 24,
    src: '/img/Cards/PNG/7_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk7'
  },
  {
    id: 25,
    src: '/img/Cards/PNG/7_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd7'
  },
  {
    id: 26,
    src: '/img/Cards/PNG/7_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd7'
  },
  {
    id: 27,
    src: '/img/Cards/PNG/7_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk7'
  },
  {
    id: 28,
    src: '/img/Cards/PNG/8_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk8'
  },
  {
    id: 29,
    src: '/img/Cards/PNG/8_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd8'
  },
  {
    id: 30,
    src: '/img/Cards/PNG/8_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd8'
  },
  {
    id: 31,
    src: '/img/Cards/PNG/8_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk8'
  },
  {
    id: 32,
    src: '/img/Cards/PNG/9_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk9'
  },
  {
    id: 33,
    src: '/img/Cards/PNG/9_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd9'
  },
  {
    id: 34,
    src: '/img/Cards/PNG/9_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd9'
  },
  {
    id: 35,
    src: '/img/Cards/PNG/9_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk9'
  },
  {
    id: 36,
    src: '/img/Cards/PNG/ace_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkA'
  },
  {
    id: 37,
    src: '/img/Cards/PNG/ace_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdA'
  },
  {
    id: 38,
    src: '/img/Cards/PNG/ace_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdA'
  },
  {
    id: 39,
    src: '/img/Cards/PNG/ace_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkA'
  },
  {
    id: 40,
    src: '/img/Cards/PNG/black_joker.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkJo'
  },
  {
    id: 41,
    src: '/img/Cards/PNG/black_joker2.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkJo'
  },
  {
    id: 42,
    src: '/img/Cards/PNG/jack_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdJ'
  },
  {
    id: 43,
    src: '/img/Cards/PNG/jack_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkJ'
  },
  {
    id: 44,
    src: '/img/Cards/PNG/jack_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkJ'
  },
  {
    id: 45,
    src: '/img/Cards/PNG/jack_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdJ'
  },
  {
    id: 46,
    src: '/img/Cards/PNG/king_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdK'
  },
  {
    id: 47,
    src: '/img/Cards/PNG/king_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkK'
  },
  {
    id: 48,
    src: '/img/Cards/PNG/king_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkK'
  },
  {
    id: 49,
    src: '/img/Cards/PNG/king_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdK'
  },
  {
    id: 50,
    src: '/img/Cards/PNG/queen_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdQ'
  },
  {
    id: 51,
    src: '/img/Cards/PNG/queen_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkQ'
  },
  {
    id: 52,
    src: '/img/Cards/PNG/queen_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkQ'
  },
  {
    id: 53,
    src: '/img/Cards/PNG/queen_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdQ'
  },
  {
    id: 54,
    src: '/img/Cards/PNG/red_joker.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdJo'
  },
  {
    id: 55,
    src: '/img/Cards/PNG/red_joker2.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdJo'
  }
];

window.Util = {
  util: this,
  shuffle: function (arr) {
    'use strict';
    var i, k, t = 0;

    for (i = arr.length - 1; i > 0; i -= 1) {
      k = Math.floor(Math.random() * i);

      //swap position
      t = arr[i];
      arr[i] = arr[k];
      arr[k] = t;
    }
  },


  Card: function Card(index) { // single card features
    'use strict';
    var self = this;

    this.html = document.createElement('div');
    this.html.style.backgroundImage = "url('/img/cardback_1.png')";
    this.html.className = 'outer_card';
    this.imgSrc = cards[index].src;
    this.value = index;
    this.html.innerHTML = '<img class="inner_card" src="' + this.imgSrc + '"/>';
    this.matched = false;
    this.selected = false;
    this.flipped = false;
    this.suit = cards[index].suit;

    this.html.addEventListener('click', function () {
      if (self.matched === false) {
        if (self.selected) {
          self.selected = false;
          self.html.classList.toggle('selected');
        } else {
          self.selected = true;
          self.html.classList.toggle('selected');
        }
      } 
    });
  }

};
