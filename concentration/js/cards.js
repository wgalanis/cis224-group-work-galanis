var cards = [
  {
    id: 0,
    src: '../img/Cards/PNG/10_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk10'
  },
  {
    id: 1,
    src: '../img/Cards/PNG/10_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd10'
  },
  {
    id: 2,
    src: '../img/Cards/PNG/10_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd10'
  },
  {
    id: 3,
    src: '../img/Cards/PNG/10_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk10'
  },
  {
    id: 4,
    src: '../img/Cards/PNG/2_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk2'
  },
  {
    id: 5,
    src: '../img/Cards/PNG/2_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd2'
  },
  {
    id: 6,
    src: '../img/Cards/PNG/2_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd2'
  },
  {
    id: 7,
    src: '../img/Cards/PNG/2_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk2'
  },
  {
    id: 8,
    src: '../img/Cards/PNG/3_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk3'
  },
  {
    id: 9,
    src: '../img/Cards/PNG/3_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd3'
  },
  {
    id: 10,
    src: '../img/Cards/PNG/3_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd3'
  },
  {
    id: 11,
    src: '../img/Cards/PNG/3_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk3'
  },
  {
    id: 12,
    src: '../img/Cards/PNG/4_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk4'
  },
  {
    id: 13,
    src: '../img/Cards/PNG/4_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd4'
  },
  {
    id: 14,
    src: '../img/Cards/PNG/4_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd4'
  },
  {
    id: 15,
    src: '../img/Cards/PNG/4_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk4'
  },
  {
    id: 16,
    src: '../img/Cards/PNG/5_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk5'
  },
  {
    id: 17,
    src: '../img/Cards/PNG/5_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd5'
  },
  {
    id: 18,
    src: '../img/Cards/PNG/5_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd5'
  },
  {
    id: 19,
    src: '../img/Cards/PNG/5_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk5'
  },
  {
    id: 20,
    src: '../img/Cards/PNG/6_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk6'
  },
  {
    id: 21,
    src: '../img/Cards/PNG/6_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd6'
  },
  {
    id: 22,
    src: '../img/Cards/PNG/6_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd6'
  },
  {
    id: 23,
    src: '../img/Cards/PNG/6_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk6'
  },
  {
    id: 24,
    src: '../img/Cards/PNG/7_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk7'
  },
  {
    id: 25,
    src: '../img/Cards/PNG/7_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd7'
  },
  {
    id: 26,
    src: '../img/Cards/PNG/7_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'r7'
  },
  {
    id: 27,
    src: '../img/Cards/PNG/7_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk7'
  },
  {
    id: 28,
    src: '../img/Cards/PNG/8_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk8'
  },
  {
    id: 29,
    src: '../img/Cards/PNG/8_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd8'
  },
  {
    id: 30,
    src: '../img/Cards/PNG/8_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd8'
  },
  {
    id: 31,
    src: '../img/Cards/PNG/8_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk8'
  },
  {
    id: 32,
    src: '../img/Cards/PNG/9_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk9'
  },
  {
    id: 33,
    src: '../img/Cards/PNG/9_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd9'
  },
  {
    id: 34,
    src: '../img/Cards/PNG/9_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rd9'
  },
  {
    id: 35,
    src: '../img/Cards/PNG/9_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bk9'
  },
  {
    id: 36,
    src: '../img/Cards/PNG/ace_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkA'
  },
  {
    id: 37,
    src: '../img/Cards/PNG/ace_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdA'
  },
  {
    id: 38,
    src: '../img/Cards/PNG/ace_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdA'
  },
  {
    id: 39,
    src: '../img/Cards/PNG/ace_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkA'
  },
  {
    id: 40,
    src: '../img/Cards/PNG/black_joker.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkJo'
  },
  {
    id: 41,
    src: '../img/Cards/PNG/black_joker2.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkJo'
  },
  {
    id: 42,
    src: '../img/Cards/PNG/jack_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdJ'
  },
  {
    id: 43,
    src: '../img/Cards/PNG/jack_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkJ'
  },
  {
    id: 44,
    src: '../img/Cards/PNG/jack_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkJ'
  },
  {
    id: 45,
    src: '../img/Cards/PNG/jack_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdJ'
  },
  {
    id: 46,
    src: '../img/Cards/PNG/king_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdK'
  },
  {
    id: 47,
    src: '../img/Cards/PNG/king_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkK'
  },
  {
    id: 48,
    src: '../img/Cards/PNG/king_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkK'
  },
  {
    id: 49,
    src: '../img/Cards/PNG/king_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdK'
  },
  {
    id: 50,
    src: '../img/Cards/PNG/queen_of_clubs.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdQ'
  },
  {
    id: 51,
    src: '../img/Cards/PNG/queen_of_diamonds.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkQ'
  },
  {
    id: 52,
    src: '../img/Cards/PNG/queen_of_hearts.png',
    flipped: 'false',
    matched: 'false',
    suit: 'bkQ'
  },
  {
    id: 53,
    src: '../img/Cards/PNG/queen_of_spades.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdQ'
  },
  {
    id: 54,
    src: '../img/Cards/PNG/red_joker.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdJo'
  },
  {
    id: 55,
    src: '../img/Cards/PNG/red_joker2.png',
    flipped: 'false',
    matched: 'false',
    suit: 'rdJo'
  }
];
