/*global window */
/*global document */
/*global console */
'use strict';

//set up the timer, starts when the app starts
var minutesLabel = document.getElementById("minutes");
var secondsLabel = document.getElementById("seconds");
var totalSeconds = 0;
var gameTimer = setInterval(setTime, 1000);

function setTime() {
  ++totalSeconds;
  secondsLabel.innerHTML = pad(totalSeconds % 60);
  minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
}

function pad(val) {
  var valString = val + "";
  if (valString.length < 2) {
    return "0" + valString;
  } else {
    return valString;
  }
}

function stopTimer(){
  clearInterval(gameTimer);
}

var ConcentrationApp;
ConcentrationApp = function () {


  function pad(val) {
    var valString = val + "";
    if (valString.length < 2) {
      return "0" + valString;
    } else {
      return valString;
    }
  }

  //set up the move counter
  var moveCounter = 0;
  var moveText;
  moveText = document.getElementById('moves');
  moveText.innerHTML = moveCounter;
  var self = this;

  //the main event
  this.deck = [];
  this.card1 = null;
  this.card2 = null;
  this.numberMatch = null;
  this.possibleMatches = null;
  this.board = document.getElementById('concentrationgame');
  this.moveCounter = 0;
  this.click = function (event) {

    // Grab event target and check to see which card was clicked

    var target = (function (t) {
      // compare event target against each card in deck, return card's reference
      for (var i = 0; i < self.deck.length; i++) {
        if (t === self.deck[i].html) {
          return self.deck[i];
        }
      }
    }(event.target));

    //Game logic
    //If card clicked is selected
    if (this.classList.contains('matched') === false) {
      if (this.classList.contains('selected') === true) {
        //If no card has been chosen yet
        if (self.card1 === null && self.card2 === null) {
          self.card1 = target;
        } else if (self.card1 !== null && self.card2 === null) {
          self.card2 = target;
          //check for match
          if ((self.card1.suit === self.card2.suit) && (self.card1.value !== self.card2.value)) {
            self.cardMatch();
            moveText.innerHTML = moveCounter += 1;
          } else {
            self.cardMisMatch();
            moveText.innerHTML = moveCounter += 1;
          }
        }
      }
    }

  };

  this.init();
};


ConcentrationApp.prototype.init = function () {
  this.numberMatch = 0;
  var i;
  for (i = 0; i < cards.length; i += 1) {
    this.deck.push(new Util.Card(i)); //push card information (new card object) into each element of the deck.
  }

  window.Util.shuffle(this.deck);

  // Get the cards into the game div
  var divadd;
  divadd = document.getElementById('concentrationgame');
  for (i = 0; i < this.deck.length; i++) {
    this.board.append(this.deck[i].html);
    this.deck[i].html.addEventListener('click', this.click);
  } // The board is loaded 
};

ConcentrationApp.prototype.cardMatch = function () {
  var self = this;
  console.log('Cards match!');
  self.possibleMatches = cards.length / 2;
  self.numberMatch += 1;
  console.log(self.numberMatch);

  window.setTimeout(function () {
    self.card1.html.classList.add('matched');
    self.card2.html.classList.add('matched');
  }, 100);

  window.setTimeout(function () {
    self.removeSelected(self);
  }, 100);

  console.log(self.card1);
  console.log(self.card2);

  if (self.numberMatch == self.possibleMatches) { // A win has occurred
    var messageBox = document.getElementById('message');
    messageBox.innerHTML = "Yay!! You win!!";
    stopTimer();
  }

};

ConcentrationApp.prototype.cardMisMatch = function () {
  var self = this;

  //change card state back to unselected
  window.setTimeout(function () {
    self.removeSelected(self);
  }, 500);


};

ConcentrationApp.prototype.removeSelected = function (app) {
  app.card1.html.classList.remove('selected');
  app.card2.html.classList.remove('selected');

  app.card1 = null;
  app.card2 = null;
};

var game;
game = new ConcentrationApp();
