window.onload = function() {
    
// initialize variables and setup page for inital view
  var clickCount = 0;
  var lastClickTime = 'never';
  var clickTime;
  var checkClickTime;
  var storeClickTime;
  var lastClickElapse = 'never';
  var timeTime = 'times';
  var timeUnit;
  var totalSeconds = 0;

  var clickPara = document.getElementById('buttonClick');
  var clickTime = document.getElementById('lastClick');
  var clickButton = document.getElementById('button');
  var clearButton = document.getElementById('clearLocalStorage');

  if (localStorage.localClickCount) {
    clickCount = localStorage.getItem('localClickCount', clickCount);
    clickCount = parseInt(clickCount);
  }

  if (localStorage.localClickTime) {
    lastClickTime = localStorage.getItem('localClickTime', lastClickTime); //This is text, not a time string
    setInterval(getElapsedTime, 100);
    getTime();
    getElapsedTime();

  } else {
    clickTime.innerHTML = 'Last click ' + lastClickTime + ' (' + lastClickElapse + ')';
  }

  if (localStorage.storedClickTime) {
    var strTime;
    strTime = localStorage.getItem('storedClickTime', checkClickTime);
    checkClickTime = new Date(strTime); // storage stores dates as strings.  This converts back to date object.
  }

  clickPara.innerHTML = 'Button clicked ' + parseInt(clickCount) + ' ' + timeTime;

  clickButton.addEventListener('click', function () {
    clickCount += 1;
    lastClickTime = getTime();
    totalSeconds = 0;
    lastClickElapse = 0;
    setInterval(getElapsedTime, 100); // start/reset timer on click
    checkClickTime = new(Date); // for continuous elapsed time calculation
    localStorage.setItem('storedClickTime', checkClickTime);
    localStorage.setItem('localClickCount', parseInt(clickCount));
    localStorage.setItem('localClickTime', lastClickTime);
    updatePage();
  });

  clearButton.addEventListener('click', function () {
    localStorage.clear(); // absolutely necessary for debugging
  });

  function updatePage() {

    if (clickCount === 1) {
      timeTime = 'time';
    } else {
      timeTime = 'times';
    }

    clickPara.innerHTML = 'Button clicked ' + clickCount + ' ' + timeTime;
    clickTime.innerHTML = 'Last click ' + lastClickTime + ' (' + lastClickElapse + ' ' + timeUnit + ' ago)';
  }

  function getTime(time) {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;

    return strTime;
  }

  function getElapsedTime() {
    ++totalSeconds;

    lastClickElapse = parseInt((Math.abs(new Date() - checkClickTime)) / 60000);

    if (lastClickElapse === 1) {
      timeUnit = 'minute';
    } else {
      timeUnit = 'minutes';
    }

    clickTime.innerHTML = 'Last click ' + lastClickTime + ' (' + lastClickElapse + ' ' + timeUnit + ' ago)';
  }
}