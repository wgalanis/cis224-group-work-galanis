/*
Exploring the waters
Journey 15
add Border
*/

function addBorder(picture) {
    var star = '*';
    var strStars = '';
    var numStars;
    var i = 0;
    var arrLength = picture.length;

    for (i = 0; i < arrLength; i += 1) {
        picture[i] = star + picture[i] + star;
    }

    numStars = picture[0].length;;
    for (i = 1; i <= numStars; i += 1) {
        strStars += star;
        console.log(strStars);
    }

    picture.push(strStars);
    picture.splice(0, 0, strStars);


    return picture;
}
