/*
Journey Begins - Number 3 - checkPalindrome  
*/

function checkPalindrome(inputString) {
    var txtArray = inputString.split("");
    var outputString = txtArray.reverse().join("");
    
    if (inputString == outputString) { 
        return true;
    }
    else {return false;}
}